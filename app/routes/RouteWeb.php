<?php

$route = new \Engine\Route();

$route->get('/', 'MainController@index');

// Admin
$route->get('/logout', 'AdminController@logout');
$route->get('/admin/login', 'AdminController@login');
$route->get('/admin/edit', 'AdminController@edit');
$route->get('/admin/add', 'AdminController@add');
