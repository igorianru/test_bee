<?php

namespace Models;

use Engine\ModelCommon;

/**
 * Class Log.
 *
 * @package Models
 */
class Log extends ModelCommon
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'users_id',
        'tasks_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('Models\Users', 'id', 'users_id');
    }

    /**
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:I',
        'updated_at' => 'datetime:Y-m-d H:I',
    ];
}
