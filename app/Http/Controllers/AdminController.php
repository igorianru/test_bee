<?php

namespace App\Controllers;

use Carbon\Carbon;
use Engine\Controller;
use Engine\Route;
use Models\Log;
use Models\Tasks;
use Models\Users;
use Services\AuthService;

/**
 * Class AdminController.
 *
 * @package App\Controllers
 */
class AdminController extends Controller
{
    /**
     * @var AuthService.
     */
    public $authServices;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->authServices = new AuthService();

        $path = (new Route())->uri('path');


        if($path !== '/admin/login' && $path !== '/admin/add' && !$this->authServices->isAuth())
            $this->redirect('/admin/login');
    }

    /**
     * Edit function.
     *
     * @return bool
     */
    public function edit()
    {
        $task   = Tasks::query()->with(['user'])->find($this->request->get('id'));
        $errors = !$this->request->isMethod('post') ? [] : $this->_validate();

        if($task && $this->request->isMethod('post') && $this->authServices->isAuth() && empty($errors)) {
            $textNew   = htmlspecialchars($this->request->get('text', ''));
            $statusNew = (int) ($this->request->get('status', 'off') === 'on');
            $userId    = $this->authServices->getCurrentUser()['id'] ?? 0;

            if($textNew != $task->text) {
                Log::query()->insert(
                    [
                        'type'       => 'edit',
                        'users_id'   => $userId,
                        'tasks_id'   => $task->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );
            }

            if($statusNew && !$task->status) {
                Log::query()->insert(
                    [
                        'type'       => 'closed',
                        'users_id'   => $userId,
                        'tasks_id'   => $task->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );
            }

            if(!$statusNew && $task->status) {
                Log::query()->insert(
                    [
                        'type'       => 'reopen',
                        'users_id'   => $userId,
                        'tasks_id'   => $task->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );
            }

            $task->name     = htmlspecialchars($this->request->get('name'));
            $task->text     = $textNew;
            $task->status   = $statusNew;
            $task->users_id = $this->request->get('users_id');

            $task->save();
            $this->redirect('/');
        } else {
            if(!$task)
                $this->redirect('/');

            return $this->render(
                'main/edit',

                [
                    'task'   => $task->toArray(),
                    'users'  => Users::query()->get()->toArray(),
                    'errors' => $errors,
                ]
            );
        }
    }

    private function _validate()
    {
        $errors = [];

        if(!$this->request->get('name'))
            $errors['name'] = 'The "Name" field is required';

        if(!$this->request->get('text'))
            $errors['text'] = 'The "Text" field is required';

        return $errors;
    }

    /**
     * Add function.
     */
    public function add()
    {
        $errors = !$this->request->isMethod('post') ? [] : $this->_validate();

        if($this->request->isMethod('post') && empty($errors)) {
            Tasks::query()->insert(
                [
                    'name'       => htmlspecialchars($this->request->get('name')),
                    'status'     => 0,
                    'text'       => htmlspecialchars($this->request->get('text', '')),
                    'users_id'   => $this->request->get('users_id'),
                    'author_id'  => $this->authServices->getCurrentUser()['id'] ?? 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
            );
            $this->redirect('/');
        } else {
            return $this->render(
                'main/add',
                ['users' => Users::query()->get()->toArray(), 'errors' => $errors]
            );
        }
    }

    /**
     * Login.
     */
    public function login()
    {
        if($this->request->isMethod('post')) {
            $isAuth = $this->authServices->auth($this->request->get('login'), $this->request->get('password'));

            if($isAuth)
                $this->redirect('/');
            else
                $this->render('main/login', ['isError' => true]);
        } else {
            $this->render('main/login');
        }
    }

    /**
     * Logout function.
     */
    public function logout()
    {
        $this->authServices->logout();
    }
}
