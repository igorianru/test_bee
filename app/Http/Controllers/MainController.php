<?php

namespace App\Controllers;

use Engine\Controller;
use Illuminate\Pagination\Paginator;
use Models\Tasks;
use Services\AuthService;

/**
 * Class MainController.
 *
 * @package App\Controllers
 */
class MainController extends Controller
{
    /**
     * @var AuthService.
     */
    public $authServices;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->authServices = new AuthService();
    }

    /**
     * Main pages, view top 10
     * @return bool
     */
    public function index()
    {
        $currentPage = $this->request->get('page', 1);
        $sortBy      = $this->request->get('sort_by', false);
        $sort        = $this->request->get('sort', false);

        Paginator::currentPageResolver(
            function() use ($currentPage) {
                return $currentPage;
            }
        );

        $tasks = Tasks::query()->with(['author', 'user', 'log.user']);

        if($sortBy && $sort) {
            if($sortBy === 'author') {
                $tasks
                    ->leftJoin('users', 'tasks.author_id', 'users.id')
                    ->orderBy('users.email', $sort);
            } elseif($sortBy === 'executor') {
                $tasks
                    ->leftJoin('users', 'tasks.users_id', 'users.id')
                    ->orderBy('users.email', $sort);
            } else {
                $tasks->orderBy($sortBy, $sort);
            }
        }

        $tasks = $tasks->groupBy('tasks.id')->select('tasks.*')->paginate(3);

        return $this->render(
            'main/index',

            [
                'tasks'  => $tasks,
                'sortBy' => $sortBy,
                'sort'   => $sort,
                'isAuth' => $this->authServices->isAuth(),
            ]
        );
    }
}
