<?php

namespace Services;

use Models\Users;

/**
 * Class AuthService.
 *
 * @package Services
 */
class AuthService {
    /**
     * @var
     */
    public $sessions;

    /**
     * AuthService constructor.
     */
    public function __construct()
    {
        $this->sessions = $_SESSION;
    }

    public function getCurrentUser()
    {
        return $this->sessions['current_user'] ?? false;
    }

    /**
     * Auth.
     *
     * @param $login
     * @param $password
     * @return bool
     */
    public function auth($login, $password)
    {
       $result = false;

        if($login && $password) {
            $user = Users::query()
                ->where([['login', '=', $login], ['password', '=', sha1($password)]])
                ->first();

            if(isset($user['id'])) {
                $_SESSION['current_user'] = $user ->toArray();

                return $user->toArray();
            }
        }

        return $result;
    }

    public function logout()
    {
     unset($_SESSION['current_user']);
     header('location: /');
    }

    /**
     * Check in auth user.
     *
     * @return bool
     */
    public function isAuth()
    {
        return isset($this->sessions['current_user']['id']) && $this->sessions['current_user']['id'];
    }
}