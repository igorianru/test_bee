<?php require_once VIEWS_PATH . "/layers/html_top.php" ?>
<?php require_once VIEWS_PATH . "/block/menu.php" ?>

    <ul class="list-group">

        <table class="table table-hover">
            <tr class="row">
                <td class="col-md-1">#</td>

                <td class="col-md-3">
                    <a href="?sort_by=name&sort=<?= $sort === 'desc' ? 'asc' : 'desc' ?>" >
                        Name
                        <?php if($sortBy === 'name') : ?>
                            <i class="glyphicon glyphicon-sort-by-attributes<?= $sort === 'asc' ? '-alt' : '' ?>"></i>
                        <?php endif;?>
                    </a>
                </td>

                <td class="col-md-2">
                    <a href="?sort_by=status&sort=<?= $sort === 'desc' ? 'asc' : 'desc' ?>" >
                        Status
                        <?php if($sortBy === 'status') : ?>
                            <i class="glyphicon glyphicon-sort-by-attributes<?= $sort === 'asc' ? '-alt' : '' ?>"></i>
                        <?php endif;?>
                    </a>
                </td>

                <td class="col-md-2">
                    <a href="?sort_by=author&sort=<?= $sort === 'desc' ? 'asc' : 'desc' ?>" >
                        Author
                        <?php if($sortBy === 'author') : ?>
                            <i class="glyphicon glyphicon-sort-by-attributes<?= $sort == 'asc' ? '-alt' : '' ?>"></i>
                        <?php endif;?>
                    </a>
                </td>

                <td class="col-md-2">
                    <a href="?sort_by=executor&sort=<?= $sort === 'desc' ? 'asc' : 'desc' ?>" >
                        Executor
                        <?php if($sortBy === 'executor') : ?>
                            <i class="glyphicon glyphicon-sort-by-attributes<?= $sort === 'asc' ? '-alt' : '' ?>"></i>
                        <?php endif;?>
                    </a>
                </td>

                <td class="col-md-2"></td>
            </tr>
        </table>

        <?php foreach($tasks ?? [] as $task) : ?>
            <li class="list-group-item panel panel-info" style="padding: 0px 15px;">
                <div class="row panel-heading">
                    <div class="col-md-1">
                        <?=$task['id']?>
                    </div>

                    <div class="col-md-3">
                        <?=$task['name']?>
                    </div>
                    <div class="col-md-2">
                        <b>Status:</b> <?=!$task['status'] ? 'active' : 'closed'?>
                    </div>

                    <div class="col-md-2">
                        <b>Author:</b> <?=$task['author']['email'] ?? 'unknown'?>
                    </div>

                    <div class="col-md-2">
                        <b>Executor:</b> <?=$task['user']['email']?>
                    </div>

                    <div class="col-md-2">
                        <div class="text-right">
                            <a href="/admin/edit?id=<?=$task['id']?>" class="btn btn-success">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <p class="panel-body">
                    <?=$task['text'] ? $task['text'] : 'No description'?>

                    <?php foreach($task['log'] ?? [] as $log) : ?>
                        <br />
                        <span>
                            <b>Action:</b>
                            <?=$log['type']?>;
                            By <?=$log['user']['name']?>
                            at <?=$log['created_at']?>
                        </span>
                    <?php endforeach; ?>
                </p>
            </li>
        <?php endforeach; ?>
    </ul>

<?php if($tasks->count() > 3) : ?>
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <?php foreach(range(1, $tasks->lastPage()) as $page) : ?>
                <li class="<?=$tasks->currentPage() == $page ? 'active' : ''?>">
                    <a href="<?=$tasks->url($page)?>" aria-label="Previous">
                        <span aria-hidden="true"><?=$page?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav>
<?php endif; ?>

<?php require_once VIEWS_PATH . "/layers/html_bottom.php" ?>