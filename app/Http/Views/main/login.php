<?php require_once VIEWS_PATH . "/layers/html_top.php" ?>
<?php require_once VIEWS_PATH . "/block/menu.php" ?>

<div class="col-lg-offset-4">
    <div class="col-md-4">
        <h3>Login page</h3>

        <?php if(isset($isError)) : ?>
            <div class="alert alert-danger">Invalid username or password</div>
        <?php endif; ?>

        <form action="/admin/login" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Login</label>
                <input type="text" name="login" class="form-control" id="exampleInputLogin" placeholder="Login">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="Password">
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </form>
    </div>
</div>
<?php require_once VIEWS_PATH . "/layers/html_bottom.php" ?>