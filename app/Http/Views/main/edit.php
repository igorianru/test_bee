<?php require_once VIEWS_PATH . "/layers/html_top.php" ?>
<?php require_once VIEWS_PATH . "/block/menu.php" ?>

<div>
    <?php foreach($errors ?? [] as $e) : ?>
        <div class="alert alert-danger"><?= $e ?></div>
    <?php endforeach; ?>

    <form action="/admin/edit?id=<?=$task['id']?>" method="post">
        <div class="form-group">
            <label for="exampleInputName">Name</label>

            <input
                name="name"
                type="text"
                class="form-control"
                id="exampleInputName"
                placeholder="Name"
                value="<?=$task['name']?>"
            />
        </div>

        <div class="form-group">
            <label for="exampleInputUser">User</label>

            <select class="form-control" autocomplete="off" name="users_id">
                <?php foreach($users as $user) : ?>
                    <option value="<?=$user['id']?>"  <?=$user['id'] === $task['users_id'] ? 'selected="selected"' : '' ?>>
                        <?=$user['name'] ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <label for="exampleInputText">Text</label>
            <textarea class="form-control"  id="exampleInputText" name="text"><?=$task['text'] ?></textarea>
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="status" <?= $task['status'] ? 'checked' : '' ?> />
                    Is Complete
                </label>
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class=" btn btn-success">Save</button>
        </div>
    </form>
</div>

<?php require_once VIEWS_PATH . "/layers/html_bottom.php" ?>